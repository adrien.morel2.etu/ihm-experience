from PyQt5.QtWidgets import QDialog, QPushButton, QFormLayout, QLineEdit, QLabel, QComboBox
from PyQt5.QtWidgets import QMainWindow

from widgets.BubbleWidget import BubbleWidget
from widgets.NormalWidget import NormalWidget
from widgets.RopeWidget import RopeWidget


class ExpSetup(QDialog):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Setup Expérience")
        layout = QFormLayout()
        but_val = QPushButton("Valider")
        but_val.clicked.connect(self.launch_exp)
        txt = QLabel("Numéro d'utilisateur")
        self.num_ut = QLineEdit()
        layout.addRow(txt, self.num_ut)
        txt = QLabel("Technique")
        self.technique = QComboBox()
        self.technique.addItems({"Normal", "Bubble", "Rope"})
        layout.addRow(txt, self.technique)
        txt = QLabel("Densité")
        self.densite = QComboBox()
        self.densite.addItems({"30", "60", "90"})
        layout.addRow(txt, self.densite)
        txt = QLabel("Tailles")
        self.tailles = QComboBox()
        self.tailles.addItems({"6", "12", "18"})
        layout.addRow(txt, self.tailles)
        txt = QLabel("Nombre de répétitions")
        self.nb_rep = QLineEdit()
        layout.addRow(txt, self.nb_rep)
        layout.addWidget(but_val)
        self.setLayout(layout)

    def launch_exp(self):
        # self.hide()
        self.window = QMainWindow()
        self.window.setFixedHeight(800)
        self.window.setFixedWidth(1024)
        print(self.num_ut.text())
        b = NormalWidget(self.num_ut.text(), self.densite.currentText(), self.tailles.currentText(), self.nb_rep.text())
        tmptxt = self.technique.currentText()
        if tmptxt == "Bubble":
            b = BubbleWidget(self.num_ut.text(), self.densite.currentText(), self.tailles.currentText(), self.nb_rep.text())
        elif tmptxt == "Rope":
            b = RopeWidget(self.num_ut.text(), self.densite.currentText(), self.tailles.currentText(), self.nb_rep.text())
        self.window.setCentralWidget(b)
        self.window.show()
