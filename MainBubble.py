from PyQt5.QtWidgets import QApplication
import sys

from ExpSetup import ExpSetup


def start():
    app = QApplication(sys.argv)
    ex = ExpSetup()
    ex.show()
    app.exec()


if __name__ == '__main__':
    start()
