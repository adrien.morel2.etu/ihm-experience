import math
import random
import sys
import csv


def main():
    nb_cibles = int(sys.argv[1])
    taille = int(sys.argv[2])
    esp = int(sys.argv[3])
    res = [(random.randint(0 + taille, 1024 - taille), random.randint(0 + taille, 800 - taille))]
    i = 0
    while i < nb_cibles:
        x_tmp = random.randint(0 + taille, 1024 - taille)
        y_tmp = random.randint(0 + taille, 800 - taille)
        for x, y in res:
            if math.dist([x_tmp, y_tmp], [x, y]) < taille + esp:
                i -= 1
                break
            elif res[-1] == (x, y):
                res.append((x_tmp, y_tmp))
                break
        i += 1
    f = open(f'resources/{nb_cibles}_{taille}_{esp}.csv', 'w')
    writer = csv.writer(f)
    for (x, y) in res:
        row = [x, y, taille]
        writer.writerow(row)


if __name__ == '__main__':
    main()
