from PyQt5.QtGui import QPainter, QColor, QPen, QBrush
from PyQt5.QtCore import Qt, QPoint


class Target:
    defaultCol = Qt.black
    highlightCol = Qt.green
    toSelectCol = Qt.blue

    def __init__(self, x, y, size):
        self.x = x
        self.y = y
        self.size = size
        self.toSelect = False
        self.highlighted = False

    def paint(self, qp: QPainter):
        qp.setBrush(QBrush(self.defaultCol, Qt.SolidPattern))
        if self.highlighted:
            qp.setBrush(QBrush(self.highlightCol, Qt.SolidPattern))
        if self.toSelect:
            qp.setBrush(QBrush(self.toSelectCol, Qt.SolidPattern))
        qp.setRenderHint(QPainter.Antialiasing)
        qp.drawEllipse(QPoint(self.x, self.y), self.size / 2, self.size / 2)
