import math

from PyQt5.QtGui import QPainter, QColor, QPen, QBrush
from PyQt5.QtCore import Qt, QPoint
from typing import List

from Target import Target
T = List[Target]


class BubbleCursor:
    defaultCol = Qt.lightGray

    def __init__(self, targets: T):
        self.x = 0
        self.y = 0
        self.size = 25
        self.targets = targets
        self.closest = None

    def paint(self, qp: QPainter):
        qp.setBrush(QBrush(self.defaultCol, Qt.SolidPattern))
        qp.setRenderHint(QPainter.Antialiasing)
        qp.drawEllipse(QPoint(self.x, self.y), self.size, self.size)

    def move(self, x, y):
        self.x = x
        self.y = y
        res = self.targets[0]
        tmp = self.dist_avec(self.targets[0])
        for t in self.targets:
            dist = self.dist_avec(t)
            t.toSelect = False
            if dist < tmp:
                res = t
                tmp = dist
        self.size = tmp
        self.closest = res
        res.toSelect = True

    def dist_avec(self, target: Target):
        return math.dist([self.x, self.y], [target.x, target.y]) - (target.size / 2)
