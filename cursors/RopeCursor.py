import math

from PyQt5.QtGui import QPainter, QBrush, QColor, QPen

from cursors.BubbleCursor import BubbleCursor
from PyQt5.QtCore import Qt, QLine, QLineF


class RopeCursor(BubbleCursor):
    def __init__(self, targets):
        super().__init__(targets)

    def paint(self, qp: QPainter):
        if self.closest:
            qp.setPen(QPen(Qt.darkGreen, 2))
            line = QLineF(self.x, self.y, self.closest.x, self.closest.y)
            line.setLength(math.dist([self.x, self.y], [self.closest.x, self.closest.y]) - self.closest.size / 2)
            qp.drawLine(line)
