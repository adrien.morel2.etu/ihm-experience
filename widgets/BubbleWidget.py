from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import *

from cursors.BubbleCursor import BubbleCursor
from Target import Target
import csv
import random
import datetime


class BubbleWidget(QWidget):
    def __init__(self, id_pers, nb_cibles, taille, nb_rep):
        super().__init__()
        self.id_pers = id_pers
        self.idx_cibles = int(nb_rep)
        self.nb_cibles = int(nb_cibles)
        self.taille = taille
        self.targets = []
        file = open(f'resources/{nb_cibles}_{taille}_20.csv')
        csvreader = csv.reader(file)
        self.csvwriter = csv.writer(open(f'results/res.csv', 'a+'))
        for row in csvreader:
            t = Target(int(row[0]), int(row[1]), int(row[2]))
            self.targets.append(t)
        file.close()
        self.setMouseTracking(True)
        self.cursor = BubbleCursor(self.targets)
        self.targhigh = self.targets[self.idx_cibles]
        self.targhigh.highlighted = True
        self.time = datetime.datetime.now()

    def paintEvent(self, a0: QPaintEvent) -> None:
        p = QPainter(self)
        for t in self.targets:
            t.paint(p)
        self.cursor.paint(p)

    def mouseMoveEvent(self, mm: QMouseEvent):
        self.cursor.move(mm.x(), mm.y())
        self.update()

    def mousePressEvent(self, mm: QMouseEvent):
        if self.idx_cibles == 1:
            self.hide()
        is_right_cible = False
        if self.targhigh == self.cursor.closest:
            is_right_cible = True
            self.idx_cibles -= 1
        fin = datetime.datetime.now()
        print("Temps : ", (fin - self.time).total_seconds() * 1000, "ms")
        row = [self.id_pers, 'bubble', self.nb_cibles, self.taille, self.idx_cibles,
               (fin - self.time).total_seconds() * 1000, is_right_cible]
        self.time = fin
        self.csvwriter.writerow(row)
        self.targhigh.highlighted = False
        self.targhigh = self.targets[self.idx_cibles]
        self.targhigh.highlighted = True
        self.update()
